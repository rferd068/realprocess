# environment

Go to colab: https://colab.research.google.com/

Create notebook and upload datasets. The datasets will be only available in the runtime.

You can also use Jupyter Python IDE for offline option.

## get the dataset 

The raw datasets  and cleaned datasets can be found in the following links: 

https://drive.google.com/drive/folders/1EwTSgZzd93Q7KkkHTGZhVZhAO2WIHTKG?usp=sharing

or,

https://data.world/turba/realprocess/workspace/dataset?agentid=turba&datasetid=realprocess

For the full dataset email at: rferd068@uottawa.ca


## create the training data

Run Train.py file to create the clean training dataset

## create the test data

Run Test.py file to create the clean test dataset

## model evaluation
Run model.py and pass the dataset created from training and test phase to evaluate the classifiers.